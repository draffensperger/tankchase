/*
 * TankChaseMain.java
 *
 * Created on December 2, 2001, 6:08 PM
 */

package tankchase;

import tankchase.ui.*;
import tankchase.data.*;

/**
 *
 * @author  dave
 * @version 
 */
public class TankChaseMain {
	private static TankChaseFrame mainFrame;
	
	public static void main (String args[]) {
		mainFrame = new TankChaseFrame();
		mainFrame.setGame(new MissionOne(mainFrame.getGameObjectsDisplayer()));
		mainFrame.getGame().startSimulator();
		((MissionOne) mainFrame.getGame()).loopBackgroundMusic();
		mainFrame.show();
	}
	
	public static TankChaseFrame getMainFrame() {
		return mainFrame;
	}
}

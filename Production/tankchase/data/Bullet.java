/*
 * Tank.java
 *
 * Created on December 8, 2001, 8:44 PM
 */

package tankchase.data;

import java.awt.geom.*;

/**
 *
 * @author  dave
 * @version 
 */
public class Bullet extends tankchase.data.GameObject {
	public Bullet() {
	}

	public void simulate(long milleseconds) {
		if (!isFullyWithinPlayingField()) {
			getGame().removeGameObject(this);
		}

		super.simulate(milleseconds);
	}
	public void collidedWith(GameObject other) {
		getGame().removeGameObject(this);
	}
}

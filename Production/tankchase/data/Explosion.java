/*
 * Explosion.java
 *
 * Created on 22 January 2002, 10:33
 */
package tankchase.data;

/**
 *
 * @author  Dave
 */
public class Explosion extends GameObject {
	private long lifeTimeLeft;
    public Explosion(long lifeTimeLeft) {
		this.lifeTimeLeft = lifeTimeLeft;
    }
	public void simulate(long ms) {
		lifeTimeLeft -= ms;
		
		if (lifeTimeLeft < 0) {
			getGame().removeGameObject(this);
		}
	}
}

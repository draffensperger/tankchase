/*
 * Game.java
 *
 * Created on December 2, 2001, 6:18 PM
 */

package tankchase.data;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.awt.image.*;
import java.awt.geom.*;

/**
 *
 * @author  dave
 * @version 
 */
public class Game extends Observable {
	private static final long SIMULATOR_TICK_DELAY = 200;
	public static final boolean BEFORE_SIMULATE_CALLS = false;
	public static final boolean AFTER_SIMULATE_CALLS = true;
	
	private KeyListener keyListener;
	private Image backgroundImage;
	private Vector gameObjects;
	private Simulator simulatorThread;
	private Shape playingField;
	private Vector humanGameObjects;

	public Game() {
		gameObjects = new Vector();
		simulatorThread = new Simulator();
		humanGameObjects = new Vector();
	}
	public void setBackgroundImage(Image backgroundImage) {
		this.backgroundImage = backgroundImage;
	}
	public Image getBackgroundImage() {
		return backgroundImage;
	}
	public static long getSimulatorTickDelay() {
		return SIMULATOR_TICK_DELAY;
	}
	public void setPlayingField(Shape playingField) {
		this.playingField = playingField;
	}
	public Shape getPlayingField() {
		return playingField;
	}
	public Vector getHumanGameObjects() {
		return humanGameObjects;
	}
	public void setHumanGameObjects(Vector humanGameObjects) {
		this.humanGameObjects = humanGameObjects;
	}
	public void markGameObjectAsHuman(GameObject object) {
		if (!getGameObjects().contains(object)) {
			throw new IllegalArgumentException("All GameObjects marked as human must be contained in the gameObjects vector.");
		} else {
			this.humanGameObjects.addElement(object);
		}
	}
	public void setGameObjects(Vector gameObjects) {
		this.gameObjects = gameObjects;
		
		for (int i = 0; i < gameObjects.size(); i++) {
			((GameObject)gameObjects.elementAt(i)).setGame(this);
		}
	}
	public Vector getGameObjects() {
		return gameObjects;
	}	
	public void addGameObject(GameObject newObject) {
		synchronized (gameObjects) {
			newObject.setGame(this);
			gameObjects.addElement(newObject);
		}
	}
	public void removeGameObject(GameObject objToRemove) {
		synchronized (gameObjects) {
			getGameObjects().removeElement(objToRemove);
		}
	}
	public void startSimulator() {
		simulatorThread.startSimulator();
	}
	public void stopSimulator() {
		simulatorThread.stopSimulator();
	}
	public Thread getSimulatorThread() {
		return simulatorThread;
	}
	public void simulateOneTick(long milleseconds) {
		synchronized (gameObjects) {
			setChanged();
			notifyObservers(new Boolean(BEFORE_SIMULATE_CALLS));
			Vector objs = getGameObjects();

			for (int i = 0; i < objs.size(); i++) {
				((GameObject)objs.elementAt(i)).simulate(milleseconds);
			}
			
			// The separate loop is needed to avoid ArrayIndexOutOfBounds
			// Exceptions when the simulate calls above remove game objects.
			for (int i = 0; i < objs.size(); i++) {
				for (int j = 0; j < getGameObjects().size(); j++) {
					if (j != i) {
						GameObject obj1 = (GameObject) objs.elementAt(i);
						GameObject obj2 = (GameObject) objs.elementAt(j);										
						Area obj1Area = new Area(obj1.getShape());
						Area obj2Area = new Area(obj2.getShape());

						Area initialObj1Area = (Area) obj1Area.clone();
						obj1Area.subtract(obj2Area);					
						if (!obj1Area.equals(initialObj1Area)) {
							obj1.collidedWith(obj2);
							obj2.collidedWith(obj1);
						}
					}
				}
			}

			setChanged();
			notifyObservers(new Boolean(AFTER_SIMULATE_CALLS));
		}
	}
	public void setKeyListener(KeyListener keyListener) {
		this.keyListener = keyListener;
	}
	public KeyListener getKeyListener() {
		return keyListener;
	}
	class Simulator extends Thread {	
		private boolean isSimulatorStopped = false;
				
		public Simulator() {
			setName("Games simulator");
		}
		
		public void startSimulator() {
			start();
		}
		
		public void stopSimulator() {
			isSimulatorStopped = true;
		}
		
		public void run() {
			long tickStartTime;
			
			while (!isSimulatorStopped) {
				try {
					tickStartTime = System.currentTimeMillis();
					Thread.sleep(getSimulatorTickDelay());
					simulateOneTick(System.currentTimeMillis() - tickStartTime);
				} catch (InterruptedException exception) {
					exception.printStackTrace();
				}
			}
		}
	}
}

/*
 * GameObject.java
 *
 * Created on November 30, 2001, 9:46 PM
 */

package tankchase.data;

import java.awt.image.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.Hashtable;
import java.applet.*;

/**
 *
 * @author  dave
 * @version 
 */
public class GameObject implements Cloneable {
	private Shape shape;
	private Color color;
	private double direction = 0;
	private double speed;
	private Game game;
	private double angleToRotateShape = 0;
	private Image image;
	private double facingDirection;
	private AudioClip cloneAudioClip;

	public GameObject() {
	}
	public void setCloneAudioClip(AudioClip cloneAudioClip) {
		this.cloneAudioClip = cloneAudioClip;
	}
	public AudioClip getCloneAudioClip () {
		return cloneAudioClip;
	}
	public void collidedWith(GameObject other) {}
	public void setGame(Game game) {
		this.game = game;
	}
	public Game getGame() {
		return game;
	}	
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	public Shape getShape() {
		return shape;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public Color getColor() {
		return color;
	}
	public void setDirection(double direction) {
		if (direction < 0) {
			this.direction = Math.PI * 2 + direction % (Math.PI * 2);
		} else {
			this.direction = direction % (Math.PI * 2);
		}
	}
	public void rotate(double theta) {
		setDirection(getDirection() + theta);
		angleToRotateShape += theta;
		setFacingDirection(getFacingDirection() + theta);
	}
	public double getDirection() {
		return direction;
	}
	public void reverse(double deltaSpeed) {
		this.speed -= deltaSpeed;
	}
	public void accelerate(double deltaSpeed) {
		this.speed += deltaSpeed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;		
	}
	public double getSpeed() {
	  return speed;
	}
	public void move(double distance) {
		AffineTransform moveTransform = AffineTransform.getTranslateInstance(distance * Math.cos(getDirection()), 
			distance * -Math.sin(getDirection()));

		setShape(moveTransform.createTransformedShape(getShape()));
	}
	public void simulate(long milleseconds) {
		if (angleToRotateShape != 0) {
			AffineTransform rotate = 
				AffineTransform.getRotateInstance(-angleToRotateShape, getShape().getBounds2D().getCenterX(),
				getShape().getBounds2D().getCenterY());

			setShape(rotate.createTransformedShape(getShape()));
			angleToRotateShape = 0;
		}

		move(speed * milleseconds / 1000);	
	}
	public boolean isFullyWithinPlayingField() {
		Area playingFieldArea = new Area(getGame().getPlayingField());
		Area shapeArea = new Area(getShape());
		shapeArea.subtract(playingFieldArea);

		return shapeArea.isEmpty();
	}
	public void ensureInsidePlayingField() {
		Rectangle2D bounds = getShape().getBounds2D();
		Rectangle2D field = getGame().getPlayingField().getBounds2D();
		AffineTransform moveIntoField = new AffineTransform();			
		if (bounds.getX() < 0) {
			moveIntoField.translate(-bounds.getX(), 0);
		} else if (bounds.getX() >  field.getWidth() - bounds.getWidth()) {
			moveIntoField.translate(field.getWidth() - bounds.getX() 
									- bounds.getWidth(), 0);
		}			
		if (bounds.getY() < 0) {
			moveIntoField.translate(0, -bounds.getY());
		} else if (bounds.getY() > field.getHeight() - bounds.getHeight()) {
			moveIntoField.translate(0, field.getHeight() - bounds.getY() 
									- bounds.getHeight());
		}			
		setShape(moveIntoField.createTransformedShape(getShape()));
	}
	public void setImage(Image image) {
		this.image = image;
	}

	public Image getImage() {
		return image;
	}
	public Object clone() {
		if (cloneAudioClip != null) {
			cloneAudioClip.play();
		}
		
		try {
			GameObject clonedGameObject = (GameObject)super.clone();
			if (shape != null) {
				clonedGameObject.shape = new Area(shape);
			}
			if (color != null) {
				clonedGameObject.color = new Color(color.getRGB());
			}
			return clonedGameObject;
		} catch (CloneNotSupportedException err) {
			err.printStackTrace();
			return null;
		}
	}
	public void setFacingDirection(double facingDirection) {
		if (facingDirection < 0) {
		  this.facingDirection = Math.PI * 2 + facingDirection % (Math.PI * 2);
		} else {
		  this.facingDirection = facingDirection % (Math.PI * 2);
		}
	}	
	public double getFacingDirection() {
		return facingDirection;
	}
}

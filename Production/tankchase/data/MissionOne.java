/*
 * MissionOne.java
 *
 * Created on December 2, 2001, 6:19 PM
 */

package tankchase.data;

import tankchase.TankChaseMain;
import tankchase.data.*;
import tankchase.util.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.awt.geom.*;
import java.applet.*;

/**
 *
 * @author  dave
 * @version 
 */
public class MissionOne extends Game {
	private static final long SIMULATOR_TICK_DELAY = 75;
	private static final Shape PLAYING_FIELD = new Rectangle(0, 0, 400, 400);
	private static final double TANK_ROTATION_INCREMENT = Math.PI / 16;
	private static final double TANK_SPEED_INCREMENT = 5;
	private static final String SOUNDS_ROOT_DIR = "sounds/";
	private static final String IMAGES_ROOT_DIR = "images/";
	private static final String TANK_IMAGE_FILENAME = IMAGES_ROOT_DIR + "tank.gif";
	private static final String BG_IMAGE_FILENAME = IMAGES_ROOT_DIR + "star.jpg";
	private static final String TANK_IMG_FILE_PREFIX = IMAGES_ROOT_DIR + "tank";
	private static final String TANK_IMG_FILE_SUFFIX = ".gif";
	private static final String BULLET_IMG_FILE_NAME = IMAGES_ROOT_DIR + "missile.gif";
	private static final String TANK_EXPLOSION_FILE_NAME = IMAGES_ROOT_DIR + "explosion.jpg";
	private static final String BULLET_AUDIO_FILE = SOUNDS_ROOT_DIR + "chung.wav";
	private static final String EXPLOSION_AUDIO_FILE = SOUNDS_ROOT_DIR + "explosion.wav";
	private static final String BACKGROUND_AUDIO_FILE = SOUNDS_ROOT_DIR + "music.mid";
	private static final int NUM_PLAYERS = 2;	
	private static final long TANK_EXPLOSION_LIFE = 250;
	private Vector humanTanks;
	private Component imageDestination;
	
	public static double getTankRotationIncrement() {
		return TANK_ROTATION_INCREMENT;
	}
	public static double getTankSpeedIncrement() {
		return TANK_SPEED_INCREMENT;
	}
	public static int getNumPlayers() {
		return NUM_PLAYERS;
	}
	public static String getTankImageFilename() {
		return TANK_IMAGE_FILENAME;
	}
	public MissionOne(Component imageDestination) {				 
		Vector gameObjects = new Vector();
		this.imageDestination = imageDestination;	
			
		try {
			setBackgroundImage(ImageUtilities.loadImage(TankChaseMain.class,
							   BG_IMAGE_FILENAME, imageDestination));			
			setPlayingField(new Rectangle(0, 0, 
							getBackgroundImage().getWidth(null), 
							getBackgroundImage().getHeight(null)));
		} catch (IOException ioe) {			
			setPlayingField(PLAYING_FIELD);
		}
		
		humanTanks = new Vector();
		Tank humanTank;
		for (int i = 0; i < getNumPlayers(); i++) {
			humanTank = createHumanTank(
					TANK_IMG_FILE_PREFIX + i + TANK_IMG_FILE_SUFFIX,
					BULLET_IMG_FILE_NAME);				
			humanTanks.addElement(humanTank);			
			gameObjects.addElement(humanTank);
		}
		
		setKeyListener(new ControlHumanTanksKeyAdapter());
		setGameObjects(gameObjects);
	}
	public Component getImageDestination() {
		return imageDestination;
	}
	public static long getSimulatorTickDelay() {
		return SIMULATOR_TICK_DELAY;
	}
	public Vector getHumanTanks() {
		return humanTanks;
	}
	public void loopBackgroundMusic() {
		try {			
			AudioUtilities.getAudioClip(TankChaseMain.class,
									    BACKGROUND_AUDIO_FILE).loop();
		} catch (IOException err) {}
	}
	private Tank createHumanTank(String imgFileName, String bulletImgFileName) {
		Tank tank = new Tank();
		tank.setDirection(Math.PI / 2);
		try {
			tank.setImage(ImageUtilities.loadImage(TankChaseMain.class, 
					imgFileName, imageDestination));
			tank.setShape(new Rectangle(0, 0,
						  tank.getImage().getHeight(null), 
						  tank.getImage().getWidth(null)));
		} catch (IOException ioe) {
			Polygon tankShape = new Polygon();
			tankShape.addPoint(0, 20);
			tankShape.addPoint(15, 0);
			tankShape.addPoint(30, 20);
			tankShape.translate(100, 100);
			tank.setShape(tankShape);
		}		
		
		Bullet bulletPrototype = new Bullet();					
		
		try {
			bulletPrototype.setCloneAudioClip(AudioUtilities.getAudioClip(
					TankChaseMain.class, BULLET_AUDIO_FILE));					
		} catch (Exception err) {}
		
		try {
			bulletPrototype.setImage(ImageUtilities.loadImage(
					TankChaseMain.class, BULLET_IMG_FILE_NAME, 
					imageDestination));
			bulletPrototype.setShape(new Rectangle(0, 0, 
					bulletPrototype.getImage().getWidth(null), 
					bulletPrototype.getImage().getHeight(null)));
		} catch (IOException ioe) {
			Polygon bulletShape = new Polygon();
			bulletShape.addPoint(0, 4);
			bulletShape.addPoint(1, 0);
			bulletShape.addPoint(2, 4);
			bulletPrototype.setShape(bulletShape);
		}
		bulletPrototype.setColor(Color.red);
		bulletPrototype.setSpeed(100);
		
		tank.setBulletPrototype(bulletPrototype);		
		
		Explosion explosionPrototype = new Explosion(TANK_EXPLOSION_LIFE);
		try {
			explosionPrototype.setImage(ImageUtilities.loadImage(
					TankChaseMain.class, TANK_EXPLOSION_FILE_NAME,
					imageDestination));
			explosionPrototype.setShape(new Rectangle(0, 0, 
					explosionPrototype.getImage().getWidth(null),
					explosionPrototype.getImage().getHeight(null)));
		} catch (IOException ioe) {
			explosionPrototype.setShape(new Rectangle(
					tank.getShape().getBounds()));
		}
		
		try {
			explosionPrototype.setCloneAudioClip(AudioUtilities.getAudioClip(
					TankChaseMain.class, EXPLOSION_AUDIO_FILE));
		} catch (IOException err) {	}
		
		explosionPrototype.setColor(Color.white);
		explosionPrototype.setSpeed(0);
		tank.setExplosionPrototype(explosionPrototype);
		
		tank.setFacingDirection(tank.getDirection());
		tank.setColor(Color.black);
		tank.setSpeed(30);
		AffineTransform move = AffineTransform.getTranslateInstance(Math.random() 
				* (getPlayingField().getBounds().getWidth() 
				   - tank.getShape().getBounds().getWidth()), Math.random()
				* (getPlayingField().getBounds().getHeight() 
				   - tank.getShape().getBounds().getHeight()));
		tank.setShape(move.createTransformedShape(tank.getShape()));
		tank.rotate(Math.random() * 2*Math.PI);
		
		return tank;
	}	 
	private class ControlHumanTanksKeyAdapter implements KeyListener {
		public void keyTyped(KeyEvent evt) {}
		public void keyReleased(KeyEvent evt) {}

		public void keyPressed(KeyEvent evt) {
			Tank player1 = (Tank)getHumanTanks().elementAt(0);
			if (player1 != null) {
				if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
					player1.rotate(-getTankRotationIncrement());
				} else if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
					player1.rotate(getTankRotationIncrement());
				} else if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
					player1.fire();
				} else if (evt.getKeyCode() == KeyEvent.VK_UP) {
					player1.accelerate(getTankSpeedIncrement());
				} else if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
					player1.reverse(getTankSpeedIncrement());
				}
			}
		}
	}
}

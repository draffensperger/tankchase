/*
 * Tank.java
 *
 * Created on December 8, 2001, 8:44 PM
 */

package tankchase.data;

import java.awt.geom.*;

/**
 *
 * @author  dave
 * @version 
 */
public class Tank extends tankchase.data.GameObject {
	private Bullet bulletPrototype;
	private Explosion explosionPrototype;
	
	public Tank() {
	}
	public void simulate(long milleseconds) {						
		if (!isFullyWithinPlayingField()) {
			ensureInsidePlayingField();
			setSpeed(getSpeed() * -0.25);			
		}

		super.simulate(milleseconds);	
	}
	public void fire() {
		if (bulletPrototype == null) {	
			throw new IllegalThreadStateException("You must set the bullet prototype before calling fire().");
		}

		Bullet bulletToFire = (Bullet)bulletPrototype.clone();
		AffineTransform bulletTransform = AffineTransform.getTranslateInstance(
				getShape().getBounds2D().getCenterX() 
				- bulletToFire.getShape().getBounds2D().getCenterX(),
				getShape().getBounds2D().getCenterY() 
				- bulletToFire.getShape().getBounds2D().getCenterY());
		bulletToFire.setShape(bulletTransform.createTransformedShape(
				bulletToFire.getShape()));
		bulletToFire.rotate(getFacingDirection());
		
		double diagonalBetweenTankCenterAndEdge = 
			Math.sqrt(Math.pow(getShape().getBounds2D().getWidth(), 2) +
			Math.pow(getShape().getBounds2D().getHeight(), 2)) / 2;
		double diagonalBetweenBulletCenterAndEdge =
			Math.sqrt(Math.pow(bulletToFire.getShape().getBounds2D().getWidth(), 2) +
			Math.pow(bulletToFire.getShape().getBounds2D().getHeight(), 2)) / 2;
	
		bulletToFire.move(diagonalBetweenTankCenterAndEdge + diagonalBetweenBulletCenterAndEdge);
		bulletToFire.setSpeed(getSpeed() + bulletPrototype.getSpeed());

		getGame().addGameObject(bulletToFire);
	}
	public void collidedWith(GameObject other) {
		if (other instanceof Bullet) {				
			if (explosionPrototype != null) {
				Explosion explosion = (Explosion) explosionPrototype.clone();
				AffineTransform moveToCenter = 
						AffineTransform.getTranslateInstance(
						getShape().getBounds2D().getCenterX() 
						- explosion.getShape().getBounds2D().getWidth() / 2,
						getShape().getBounds2D().getCenterY() 
						- explosion.getShape().getBounds2D().getHeight() / 2);
				explosion.setShape(moveToCenter.createTransformedShape(
						explosion.getShape()));
				getGame().addGameObject(explosion);
			}
			getGame().removeGameObject(this);
		}
	}
	public void setBulletPrototype(Bullet prototype) {
		bulletPrototype = prototype;
	}
	public Bullet getBulletPrototype() {
		return bulletPrototype;
	}
	public void setExplosionPrototype(Explosion prototype) {
		this.explosionPrototype = prototype;
	}
	public Explosion getExplosionPrototype() {
		return explosionPrototype;
	}
}

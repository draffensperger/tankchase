/*
 * AudioUtilities.java
 *
 * Created on 23 January 2002, 09:00
 */

package tankchase.util;

import java.applet.*;
import java.net.URL;
import java.io.IOException;

/**
 *
 * @author  Dave
 */
public class AudioUtilities {
	public static AudioClip getAudioClip(Class clazz, String fileName) 
			throws IOException {
		URL url = clazz.getResource(fileName);
		if (url != null) {
			try {
				return Applet.newAudioClip(url);
			} catch (Exception err) {
				throw new IOException("Can't load audio: " + fileName);
			}
		} else {
			throw new IOException("Can't load audio: " + fileName);
		}
	}
}

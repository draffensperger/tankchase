package tankchase.util;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.net.URL;

public class ImageUtilities {
	public static Image loadImage(String imageFileName, Component c) 
				throws IOException {
		Image imageToLoad = c.getToolkit().createImage(imageFileName);		
		return load(imageToLoad, imageFileName, c);
	}
	public static Image loadImage(Class clazz, String imageFileName,  
			Component c) throws IOException {		
		URL url = clazz.getResource(imageFileName);
		if (url != null) {			
			return load(c.getToolkit().getImage(url), imageFileName, c); 
		} else {
			throw new IOException("Can't load: " + imageFileName);
		}
	}
	public static Image load(Image img, String name, Component c) throws IOException {
		MediaTracker tracker = new MediaTracker(c);
		tracker.addImage(img, 1);
		
		try {
			tracker.waitForAll();
		} catch (InterruptedException interrupted) {
			interrupted.printStackTrace();
		}		
		
		if (tracker.isErrorAny()) {
			throw new IOException("Can't load: " + name);
		}	
		
		return img;
	}
}



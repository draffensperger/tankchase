/*
 * TestAll.java
 *
 * Created on November 30, 2001, 9:26 PM
 */

package tankchase.test;

import tankchase.test.unit.*;
import tankchase.test.features.*;
import junit.framework.*;
import junit.extensions.*;

/**
 *
 * @author  dave
 * @version 
 */
public class AllTests {
	public static void main (String[] args) {
		junit.textui.TestRunner.run (suite());
		System.exit(0);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("All Tank Chase Tests");
				
		suite.addTestSuite(GameCollisionsTest.class);
		suite.addTestSuite(MissionOneKeysTest.class);
		suite.addTestSuite(RemovingGameObjectsTest.class);
		suite.addTestSuite(TankBouncesOffEdgeTest.class);
		suite.addTestSuite(TankMovementTest.class);
		
		suite.addTestSuite(BulletTest.class);
		suite.addTestSuite(ExplosionTest.class);
		suite.addTestSuite(GameObjectTest.class);
		suite.addTestSuite(GameTest.class);
		suite.addTestSuite(ImageUtilitiesTest.class);
		suite.addTestSuite(MissionOneTest.class);
		suite.addTestSuite(TankChaseFrameTest.class);
		suite.addTestSuite(TankTest.class);
		
		return suite;
	}	 
}

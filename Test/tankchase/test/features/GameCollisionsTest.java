/*
 * GameCollisionsTest.java
 * JUnit based test
 *
 * Created on 20 January 2002, 17:25
 */                

package tankchase.test.features;

import junit.framework.*;
import tankchase.data.*;
import java.awt.*;

/**
 *
 * @author Dave
 */                                
public class GameCollisionsTest extends TestCase {    
	private boolean testGameObject1Collided;
	private boolean testGameObject2Collided;
	private Game testGame;
	private TestGameObject1 testGameObject1;
	private TestGameObject2 testGameObject2;
	
	public GameCollisionsTest(java.lang.String testName) {
		super(testName);
	}        
	public void setUp() {
		testGameObject1Collided = false;
		testGameObject2Collided = false;
		testGameObject1 = new TestGameObject1();
		testGameObject2 = new TestGameObject2();
		
		testGame = new Game();
		testGame.setPlayingField(new Rectangle(0, 0, 50, 50));
		testGame.addGameObject(testGameObject1);
		testGame.addGameObject(testGameObject2);
	}
	public void testNonCollisions() {
		testGameObject1.setShape(new Rectangle(0, 0, 10, 10));
		testGameObject2.setShape(new Rectangle(20, 20, 10, 10));
		testGame.simulateOneTick(0);
		assertTrue(!testGameObject1Collided);
		assertTrue(!testGameObject2Collided);
	}
	public void testCollisions() {
		testGameObject1.setShape(new Rectangle(0, 0, 10, 10));
		testGameObject2.setShape(new Rectangle(5, 5, 10, 10));
		testGame.simulateOneTick(0);
		assertTrue(testGameObject1Collided);
		assertTrue(testGameObject2Collided);
	}
	class TestGameObject1 extends GameObject {
		public void collidedWith(GameObject other) {
			testGameObject1Collided = true;
			assertTrue(other instanceof TestGameObject2);
			assertTrue(other == testGameObject2);
		}
	}
	class TestGameObject2 extends GameObject {
		public void collidedWith(GameObject other) {
			testGameObject2Collided = true;
			assertTrue(other instanceof TestGameObject1);
			assertTrue(other == testGameObject1);
		}
	}
}

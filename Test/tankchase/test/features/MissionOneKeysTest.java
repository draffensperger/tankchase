package tankchase.test.features;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import tankchase.ui.*;
import java.awt.geom.*;
import java.awt.event.*;

/**
 *
 * @author  dave
 * @version 
 */
public class MissionOneKeysTest extends TestCase {
	private MissionOne testGame;
	private Tank player1;
	private TankChaseFrame testFrame;
	private double originalDirection;
	private Shape originalShape;
	
	public MissionOneKeysTest(String name) {
		super (name);
	}	
	public void setUp() {
		testFrame = new TankChaseFrame();
		testGame = new MissionOne(testFrame);
		player1 = (Tank)testGame.getHumanTanks().elementAt(0);
		player1.setColor(Color.red);
		player1.setShape(new Rectangle(30, 30, 10, 20));
		player1.setDirection(Math.PI);			

		originalDirection = player1.getDirection();
		originalShape = new Area(player1.getShape());

		testFrame.setGame(testGame);
	}	
	public void testRightArrow() {
		fakeKeyEvent(KeyEvent.VK_RIGHT);
		assertTrue(player1.getDirection() < originalDirection);
		testGame.simulateOneTick(10);						
		assertTrue(!(new Area(player1.getShape())).equals(new Area(originalShape)));
	}
	public void testLeftArrow() {
		fakeKeyEvent(KeyEvent.VK_LEFT);
		assertTrue(player1.getDirection() > originalDirection);
		testGame.simulateOneTick(10);		
		assertTrue(!(new Area(player1.getShape())).equals(new Area(originalShape)));
	}
	public void testSpacebar() {
		int initialGameObjectsCount = testGame.getGameObjects().size(); 
		fakeKeyEvent(KeyEvent.VK_SPACE);
		assertTrue(testGame.getGameObjects().size() == initialGameObjectsCount + 1);
		assertTrue(testGame.getGameObjects().lastElement() instanceof Bullet);
	}
	public void testUpArrow() {
		player1.setDirection(0);
		player1.setFacingDirection(0);
		double initialHumanSpeed = player1.getSpeed();
		fakeKeyEvent(KeyEvent.VK_UP);
		assertTrue(player1.getSpeed() > initialHumanSpeed);

		player1.setDirection(0.0000000001);
		player1.setFacingDirection(0);
		initialHumanSpeed = player1.getSpeed();
		fakeKeyEvent(KeyEvent.VK_UP);
		assertTrue(player1.getSpeed() > initialHumanSpeed);
	}
	public void testDownArrow() {
		player1.setDirection(0);
		player1.setFacingDirection(0);
		double initialHumanSpeed = player1.getSpeed();
		fakeKeyEvent(KeyEvent.VK_DOWN);
		assertTrue(player1.getSpeed() < initialHumanSpeed);
		
		player1.setDirection(0.0000000001);
		player1.setFacingDirection(0);
		initialHumanSpeed = player1.getSpeed();
		fakeKeyEvent(KeyEvent.VK_DOWN);
		assertTrue(player1.getSpeed() < initialHumanSpeed);
	}
	private void fakeKeyEvent(int keyCode) {
		KeyEvent fakeEvent = new KeyEvent(testFrame, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, keyCode, KeyEvent.CHAR_UNDEFINED);
		testFrame.getKeyListener().keyPressed(fakeEvent);
	}
	public void tearDown() {
		testFrame.dispose();
	}
}

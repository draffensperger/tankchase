/*
 * RemovingGameObjectsTest.java
 * JUnit based test
 *
 * Created on 22 January 2002, 10:00
 */                

package tankchase.test.features;

import junit.framework.*;
import tankchase.data.*;
import java.awt.*;

/**
 *
 * @author Dave
 */                                
public class RemovingGameObjectsTest extends TestCase {    
    public RemovingGameObjectsTest(java.lang.String testName) {
        super(testName);
    }        
	public void testNoOutOfBoundsOnRemove() {
		Game testGame = new Game();
		testGame.setPlayingField(new Rectangle(0, 0, 50, 50));
		TestGameObject testGameObject = new TestGameObject();
		GameObject otherObject = new GameObject();
		otherObject.setShape(new Rectangle(0, 0, 10, 10));
		testGame.addGameObject(otherObject);
		testGameObject.setShape(new Rectangle(20, 20, 5, 5));
		testGame.addGameObject(testGameObject);
		
		try {
			testGame.simulateOneTick(0);
		} catch (ArrayIndexOutOfBoundsException err) {
			fail();
		}
	}
	private class TestGameObject extends GameObject {
		public void simulate(long ms) {
			getGame().removeGameObject(this);
		}
	}
}

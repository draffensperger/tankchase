/*
 * MissionOneTest.java
 *
 * Created on December 2, 2001, 5:37 PM
 */

package tankchase.test.features;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;
import java.util.*;
import java.awt.*;

/**
 *
 * @author  dave
 * @version 
 */
public class TankBouncesOffEdgeTest extends TestCase {
	private Game testGame;
	private Tank testTank;
	private double initialSpeed;
	
	public TankBouncesOffEdgeTest(String name) {
		super (name);
	}	
	public void setUp() {
		testGame = new Game();
		testTank = new Tank();
		testTank = new Tank();
		testGame.addGameObject(testTank);
		testTank.setDirection(Math.PI);
		testGame.setPlayingField(new Rectangle(0, 0, 10, 10));
		testTank.setShape(new Rectangle(0, 0, 10, 10));
	}
	public void testBounceOffEdgeRects() {		
		testTank.setSpeed(2);
		initialSpeed = testTank.getSpeed();
		testGame.simulateOneTick(1000);
		testGame.simulateOneTick(0);			
		checkDirection();
		checkSpeed();
	}
	public void testBounceOffEdgeNegativeSpeed() {		
		testTank.setSpeed(-2);
		initialSpeed = testTank.getSpeed();
		testGame.simulateOneTick(1000);
		testGame.simulateOneTick(0);			
		checkDirection();
		checkSpeed();
	}
	public void testBounceOffEdgePolys() {
		Polygon field = new Polygon();
		field.addPoint(2, 0);
		field.addPoint(0, 2);
		field.addPoint(4, 2);
		testGame.setPlayingField(field);
				
		Polygon tankShape = new Polygon();
		tankShape.addPoint(2, 0);
		tankShape.addPoint(1, 1);
		tankShape.addPoint(3, 1);		
		testTank.setShape(tankShape);
		
		testTank.setSpeed(1);
		initialSpeed = testTank.getSpeed();

		testGame.simulateOneTick(1000);
		testGame.simulateOneTick(0);		
		checkDirection();
		checkSpeed();
	}
	private void checkDirection() {
		assertTrue(testTank.getDirection() == Math.PI);
	}
	private void checkSpeed() {		
		assertTrue(Math.abs(testTank.getSpeed()) < Math.abs(initialSpeed));
		assertTrue(sign(initialSpeed) == -sign(testTank.getSpeed()) || 
			initialSpeed == 0);
	}
	private int sign(double x) {
		if (x < 0) return -1;
		else return 1;
	}
}

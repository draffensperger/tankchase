/*
 * MissionOneTest.java
 *
 * Created on December 2, 2001, 5:37 PM
 */

package tankchase.test.features;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;
import java.util.*;
import java.awt.*;

/**
 *
 * @author  dave
 * @version 
 */
public class TankMovementTest extends TestCase {
	private MissionOne testMissionOne;
	
	public TankMovementTest(String name) {
		super (name);
	}
	
	public void setUp() {
		testMissionOne = new MissionOne(new Frame());
	}
	
	public void testObjectMovementOnScreen() {
		Rectangle initialBounds = new Rectangle(
				((GameObject) testMissionOne.getGameObjects().elementAt(0))
				.getShape().getBounds());
		testMissionOne.simulateOneTick(MissionOne.getSimulatorTickDelay());
		testMissionOne.simulateOneTick(MissionOne.getSimulatorTickDelay());
		Rectangle bounds = 
				((GameObject) testMissionOne.getGameObjects().elementAt(0))
				.getShape().getBounds();
		assertTrue(!bounds.equals(initialBounds));
		assertTrue(bounds.getX() >= 0);
		assertTrue(bounds.getY() >= 0);
	}

}

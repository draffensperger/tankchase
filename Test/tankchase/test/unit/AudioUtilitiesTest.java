/*
 * AudioUtilitiesTest.java
 * JUnit based test
 *
 * Created on 23 January 2002, 09:05
 */                

package tankchase.test.unit;

import junit.framework.*;
import tankchase.util.*;
import java.io.IOException;

/**
 *
 * @author Dave
 */                                
public class AudioUtilitiesTest extends TestCase {    
    public AudioUtilitiesTest(java.lang.String testName) {
        super(testName);
    }
	public void testGetAudioClip() {
		try {
			AudioUtilities.getAudioClip(Class.class, "");
		} catch (IOException err) {
			assertTrue(!err.getMessage().equals(""));
			return;
		}
		
		fail();
	}
}

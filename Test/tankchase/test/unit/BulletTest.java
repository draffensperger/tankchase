/*
 * TankTest.java
 *
 * Created on November 30, 2001, 9:31 PM
 */

package tankchase.test.unit;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;

import java.awt.geom.*;
import java.awt.*;

/**
 *
 * @author  dave
 * @version
 */
public class BulletTest extends TestCase {
	private Game testGame;
	private Bullet testBullet;

	public BulletTest(String name) {
		super(name);
	}
	
	public void setUp() {
		testGame = new Game();
		testGame.setPlayingField(new Rectangle(0, 0, 100, 100));
		testBullet = new Bullet();
		testGame.addGameObject(testBullet);
	}
	public void testBulletDiesOnCollision() {
		GameObject otherObject = new GameObject();
		testGame.addGameObject(otherObject);
		testBullet.setShape(new Rectangle(0, 0, 10, 10));
		otherObject.setShape(new Rectangle(0, 0, 5, 5));
		testGame.simulateOneTick(0);
		assertTrue(!testGame.getGameObjects().contains(testBullet));
	}
	public void testBulletRemovedWhenHitsEdge() {		
		testGame.setPlayingField(new Rectangle(new Rectangle(0, 0, 10, 10)));
		
		testBullet.setShape(new Area(testGame.getPlayingField()));
		testBullet.setSpeed(2);
		testBullet.setDirection(Math.PI);

		testGame.simulateOneTick(1000);
		testGame.simulateOneTick(0);
		assertTrue(!testGame.getGameObjects().contains(testBullet));
	}
}
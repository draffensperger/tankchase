/*
 * ExplosionTest.java
 * JUnit based test
 *
 * Created on 22 January 2002, 10:37
 */                

package tankchase.test.unit;

import junit.framework.*;
import tankchase.data.*;
import java.awt.*;

/**
 *
 * @author Dave
 */                                
public class ExplosionTest extends TestCase {    
	private Game testGame;
	private Explosion testExplosion;
	
    public ExplosionTest(java.lang.String testName) {
        super(testName);
    }
	public void setUp() {
		testGame = new Game();
		testGame.setPlayingField(new Rectangle(0, 0, 50, 50));
		testExplosion = new Explosion(100);
		testGame.addGameObject(testExplosion);
	}
	public void testSimulate() {
		testGame.simulateOneTick(101);
		assertTrue(!testGame.getGameObjects().contains(testExplosion));
		assertTrue(testGame.getGameObjects().size() == 0);
	}
}

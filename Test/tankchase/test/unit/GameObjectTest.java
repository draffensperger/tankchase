/*
 * TankTest.java
 *
 * Created on November 30, 2001, 9:31 PM
 */

package tankchase.test.unit;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;

import java.awt.image.*;
import java.awt.geom.*;
import java.awt.*;

/**
 *
 * @author  dave
 * @version
 */
public class GameObjectTest extends TestCase {
	private GameObject testGameObject;
	private Game testGame;
	private Shape originalShape;
	private boolean testAudioClipPlayCalled;
	
	public GameObjectTest(String name) {
		super(name);
	}	
	public void setUp() {
		testGame = new Game();
		testGameObject = new GameObject();
		testGame.addGameObject(testGameObject);
		testGameObject.setShape(new Rectangle(20, 20, 10, 5));
		originalShape = new Rectangle((Rectangle)testGameObject.getShape());
		testAudioClipPlayCalled = false;
	}
	public void testAccelerate() {
		double initialSpeed = testGameObject.getSpeed();
		testGameObject.accelerate(10);
		assertTrue(testGameObject.getSpeed() == initialSpeed + 10);
	}
	private class TestAudioClip implements java.applet.AudioClip {
		public void loop() {}
		public void stop() {}
		public void play() {
			testAudioClipPlayCalled = true;
		}
	}
	public void testSetCloneAudioClip() {
		TestAudioClip clip = new TestAudioClip();
		testGameObject.setCloneAudioClip(clip);
		assertTrue(testGameObject.getCloneAudioClip() == clip);
		testGameObject.clone();
		assertTrue(testAudioClipPlayCalled);
	}
	public void testNoExceptionWhenNullAudio() {
		testGameObject.setCloneAudioClip(null);
		assertTrue(testGameObject.getCloneAudioClip() == null);
		try {
			testGameObject.clone();
		} catch (NullPointerException err) {
			fail();
		}
		assertTrue(!testAudioClipPlayCalled);
	}
	public void testReverse() {
		testGameObject.setSpeed(20);
		testGameObject.reverse(10);
		assertTrue(testGameObject.getSpeed() == 10);
		
		testGameObject.setSpeed(0);
		testGameObject.reverse(10);
		assertTrue(testGameObject.getSpeed() == -10);
	}
	public void testEnsureInsidePlayingField() {
		testGame.setPlayingField(new Rectangle(0, 0, 50, 50));
		  
		testGameObject.setShape(new Rectangle(-10, -10, 30, 30));
		testGameObject.ensureInsidePlayingField();
		assertTrue(testGameObject.getShape().getBounds().equals(
				 new Rectangle(0, 0, 30, 30)));

		testGameObject.setShape(new Rectangle(60, 60, 30, 30));
		testGameObject.ensureInsidePlayingField();
		assertTrue(testGameObject.getShape().getBounds().equals(
				 new Rectangle(20, 20, 30, 30)));

		testGameObject.setShape(new Rectangle(-10, 10, 30, 30));
		testGameObject.ensureInsidePlayingField();
		assertTrue(testGameObject.getShape().getBounds().equals(
				 new Rectangle(0, 10, 30, 30)));

		testGameObject.setShape(new Rectangle(40, 10, 30, 30));
		testGameObject.ensureInsidePlayingField();
		assertTrue(testGameObject.getShape().getBounds().equals(
				 new Rectangle(20, 10, 30, 30)));

		testGameObject.setShape(new Rectangle(10, -10, 30, 30));
		testGameObject.ensureInsidePlayingField();
		assertTrue(testGameObject.getShape().getBounds().equals(
				 new Rectangle(10, 0, 30, 30)));

		testGameObject.setShape(new Rectangle(10, 40, 30, 30));
		testGameObject.ensureInsidePlayingField();
		assertTrue(testGameObject.getShape().getBounds().equals(
				 new Rectangle(10, 20, 30, 30)));
   }
	public void testSetGame() {
		Game testGame = new Game();
		testGameObject.setGame(testGame);
		assertTrue(testGameObject.getGame() == testGame);
	}	
	public void testSetShape() {
		Shape testShape = new Rectangle(0, 0, 10, 20);
		testGameObject.setShape(testShape);		
		assertTrue(testGameObject.getShape() != null);
		assertTrue(testGameObject.getShape().equals(testShape));
	}
	public void testSetColor() {
		testGameObject.setColor(Color.red);		
		assertTrue(testGameObject.getColor().equals(Color.red));
	}
	public void testSetSpeed() {
		testGameObject.setSpeed(40);
		assertTrue(testGameObject.getSpeed() == 40);

		testGameObject.setSpeed(-10);
		assertTrue(testGameObject.getSpeed() == -10);		
	}
	public void testNoRotateWithoutSimulation() {
		assertTrue(testGameObject.getDirection() == 0);
		testGameObject.rotate(Math.PI / 2);
		assertTrue(testGameObject.getDirection() == Math.PI / 2);
		assertTrue(new Area(testGameObject.getShape()).equals(new Area(originalShape)));
	}
	public void testRotateWithSimulation() {
		double theta = Math.PI / 2;

		testGameObject.rotate(theta);
		assertTrue(testGameObject.getDirection() == theta);
		testGame.simulateOneTick(10);
		assertTrue(testGameObject.getShape().getBounds2D().getWidth() == originalShape.getBounds2D().getHeight());
		assertTrue(testGameObject.getShape().getBounds2D().getHeight() == originalShape.getBounds2D().getWidth());
		assertTrue(testGameObject.getShape().getBounds2D().getCenterX() == originalShape.getBounds2D().getCenterX());
		assertTrue(testGameObject.getShape().getBounds2D().getCenterY() == originalShape.getBounds2D().getCenterY());

		testGameObject.rotate(theta);
		assertTrue(testGameObject.getDirection() == theta * 2);
		testGame.simulateOneTick(10);
		assertTrue(testGameObject.getShape().getBounds2D().getWidth() == originalShape.getBounds2D().getWidth());
		assertTrue(testGameObject.getShape().getBounds2D().getHeight() == originalShape.getBounds2D().getHeight());
		assertTrue(testGameObject.getShape().getBounds2D().getCenterX() == originalShape.getBounds2D().getCenterX());
		assertTrue(testGameObject.getShape().getBounds2D().getCenterY() == originalShape.getBounds2D().getCenterY());

		testGameObject.rotate(Math.PI / 4);
		assertTrue(testGameObject.getDirection() == theta * 2 + Math.PI / 4);
		testGame.simulateOneTick(10);		
		double expectedSideLength = originalShape.getBounds2D().getWidth() / Math.sqrt(2) + 
			originalShape.getBounds2D().getHeight() / Math.sqrt(2);	
		assertTrue((int)testGameObject.getShape().getBounds2D().getWidth() == (int)expectedSideLength);
		assertTrue((int)testGameObject.getShape().getBounds2D().getHeight() == (int)expectedSideLength);
		assertTrue(testGameObject.getShape().getBounds2D().getCenterX() == originalShape.getBounds2D().getCenterX());
		assertTrue(testGameObject.getShape().getBounds2D().getCenterY() == originalShape.getBounds2D().getCenterY());

		originalShape = new Area(testGameObject.getShape());
		testGameObject.setDirection(0);
		for (int i = 0; i < 3; i++) {
			testGameObject.rotate(Math.PI / 3);
		}		
		testGame.simulateOneTick(0);
		assertTrue(Math.abs(testGameObject.getShape().getBounds2D().getWidth() - originalShape.getBounds2D().getWidth()) < 0.01);
		assertTrue(Math.abs(testGameObject.getShape().getBounds2D().getHeight() - originalShape.getBounds2D().getHeight()) < 0.01);
		assertTrue(testGameObject.getDirection() == Math.PI);

		Polygon testTriangle = new Polygon();
		testTriangle.addPoint(10, 0);
		testTriangle.addPoint(0, 10);
		testTriangle.addPoint(20, 10);
		testGameObject.setShape(testTriangle);
		testGameObject.rotate(Math.PI / 2);
		testGame.simulateOneTick(10);
		Polygon expectedPolygon = new Polygon();
		expectedPolygon.addPoint(5, 5);
		expectedPolygon.addPoint(15, -5);
		expectedPolygon.addPoint(15, 15);
		assertTrue(new Area(expectedPolygon).equals(new Area(testGameObject.getShape())));

		originalShape = new Area(testGameObject.getShape());
		testGameObject.rotate(Math.PI / 5);
		Area shapeArea = new Area(testGameObject.getShape());
		assertTrue(!shapeArea.isRectangular());
		assertTrue(!shapeArea.isEmpty());
		assertTrue(shapeArea.isPolygonal());
	}
	public void testSetDirection() {			
		testGameObject.setDirection(Math.PI / 2);	
		assertTrue(testGameObject.getDirection() == Math.PI / 2);

		assertTrue(testGameObject.getShape().getBounds2D().getWidth() == originalShape.getBounds2D().getWidth());
		assertTrue(testGameObject.getShape().getBounds2D().getHeight() == originalShape.getBounds2D().getHeight());

		testGameObject.setDirection(Math.PI * 3);
		assertTrue(testGameObject.getDirection() == Math.PI);

		testGameObject.setDirection(-Math.PI);
		assertTrue(testGameObject.getDirection() == Math.PI);

		testGameObject.setDirection(-Math.PI * 3);
		assertTrue(testGameObject.getDirection() == Math.PI);

		testGameObject.setDirection(Math.PI * 2);
		assertTrue(testGameObject.getDirection() == 0);
	}
	public void testSimulateNonRotationEffects() {
		testGameObject.setShape(new Rectangle(50, 50, 25, 50));
		testGameObject.setSpeed(Math.sqrt(2));
		testGameObject.setDirection(Math.PI / 4);

		Rectangle rotatedBounds = new Rectangle(testGameObject.getShape().getBounds());

		testGameObject.simulate(5000);
		assertTrue(Math.round(testGameObject.getShape().getBounds().getX()) == Math.round(rotatedBounds.getX()) + 5);
		assertTrue(Math.round(testGameObject.getShape().getBounds().getY()) == Math.round(rotatedBounds.getY()) - 5);
	}
	public void testMove() {
		testGameObject.setShape(new Rectangle(50, 50, 25, 50));
		Rectangle initialBounds = new Rectangle(testGameObject.getShape().getBounds());
		testGameObject.setDirection(Math.PI);
		testGameObject.move(5);
		assertTrue(testGameObject.getShape().getBounds().getY() == initialBounds.getY());
		assertTrue(testGameObject.getShape().getBounds().getX() == initialBounds.getX() - 5);
	}
	public void testIsFullyWithinPlayingField() {
		testGame.setPlayingField(new Rectangle(testGameObject.getShape().getBounds()));
		assertTrue(testGameObject.isFullyWithinPlayingField());

		testGameObject.setSpeed(5);
		testGame.simulateOneTick(1000);

		assertTrue(!testGameObject.isFullyWithinPlayingField());
	}
	public void testCloneWithoutColorsOrShapeSet() {
		testGameObject.setShape(null);
		testGameObject.setColor(null);
		try {
			testGameObject.clone();
		} catch (NullPointerException err) {
			fail();
		}
	}
	public void testCloneWithShapeAndColorSet() {
		testGameObject.setShape(new Rectangle(0, 0, 10, 10));
		testGameObject.setColor(Color.red);
		testGameObject.setSpeed(2);
		testGameObject.rotate(Math.PI / 5);
		GameObject testClone = (GameObject)testGameObject.clone();
		testGame.addGameObject(testClone);

		assertTrue(testClone.getColor() != testGameObject.getColor());
		assertTrue(testClone.getColor().equals(testGameObject.getColor()));
		assertTrue(testClone.getShape() != testGameObject.getShape());
		assertTrue(new Area(testClone.getShape()).equals(new Area(testGameObject.getShape())));
		assertTrue(testClone.getGame() == testGameObject.getGame());
		assertTrue(testClone.getSpeed() == testGameObject.getSpeed());
		assertTrue(testClone.getDirection() == testGameObject.getDirection());

		testGame.simulateOneTick(100);
		assertTrue(testClone.getDirection() == testGameObject.getDirection());
		assertTrue(testClone.getShape() != testGameObject.getShape());
		assertTrue(new Area(testClone.getShape()).equals(new Area(testGameObject.getShape())));
	}
	public void testSetImage() {
		BufferedImage testImage = new BufferedImage(10, 10, BufferedImage.TYPE_BYTE_GRAY);
		testGameObject.setImage(testImage);
		assertTrue(testGameObject.getImage() == testImage);
	}
	public void testSetFacingDirection() {
		testGameObject.setFacingDirection(Math.PI / 7);
		assertTrue(testGameObject.getFacingDirection() == Math.PI / 7);
		testGameObject.setFacingDirection(Math.PI * 3);
		assertTrue(testGameObject.getFacingDirection() == Math.PI);
		testGameObject.setFacingDirection(-Math.PI);
		assertTrue(testGameObject.getFacingDirection() == Math.PI);
		testGameObject.setFacingDirection(-Math.PI * 3);
		assertTrue(testGameObject.getFacingDirection() == Math.PI);		
	}
	public void testGetFacingDirection() {
		testGameObject.setFacingDirection(Math.PI / 7);
		testGameObject.setDirection(Math.PI);
		assertTrue(testGameObject.getFacingDirection() == Math.PI / 7);
	}
}

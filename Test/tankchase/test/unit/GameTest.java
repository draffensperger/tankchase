/*
 * GameTest.java
 *
 * Created on December 2, 2001, 5:37 PM
 */

package tankchase.test.unit;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.*;

/**
 *
 * @author  dave
 * @version 
 */
public class GameTest extends TestCase implements Observer {
	private Game testGame;
	private Game testSimulatorGame;
	private GameObject testGameObject;
	private int timesUpdateCalled;
	private Object[] updateArgs;
	private double speed;
	
	public GameTest(String name) {
		super (name);
	}	
	public void setUp() {
		testGame = new Game();
		testSimulatorGame = new Game();
		
		timesUpdateCalled = 0;
		updateArgs = new Object[2];
		
		testSimulatorGame.addObserver(this);
		testGameObject = new GameObject();		
		testGameObject.setShape(new Rectangle(100, 100, 25, 50));
		speed = 2;
		testGameObject.setSpeed(speed);
		testGameObject.setDirection(Math.PI);		
		testSimulatorGame.addGameObject(testGameObject);
	}
	public void testGetHumanGameObjects() {
		assertTrue(testGame.getHumanGameObjects() != null);
	}
	public void testMarkGameObjectAsHuman() {
		testGame.addGameObject(testGameObject);
		testGame.markGameObjectAsHuman(testGameObject);
		assertTrue(testGame.getHumanGameObjects().size() == 1);
		assertTrue(testGame.getHumanGameObjects().contains(testGameObject));
		
		GameObject unAddedGameObject = new GameObject();
		try {
			testGame.markGameObjectAsHuman(unAddedGameObject);
		} catch (IllegalArgumentException err) {
			assertTrue(err.getMessage() != "");
			return;
		}
		
		fail();
	}
	public void testSetGameObjects() {
		Vector gameObjects = new Vector();
		gameObjects.addElement(new GameObject());
		
		testGame.setGameObjects(gameObjects);
		
		assertTrue(!testGame.getGameObjects().isEmpty());
		assertTrue(testGame.getGameObjects().equals(gameObjects));
		assertTrue(((GameObject)testGame.getGameObjects().elementAt(0)).getGame() == testGame);
	}		
	public void testAddGameObject() {	
		assertTrue(testGame.getGameObjects() != null);
		testGame.addGameObject(new GameObject());
		
		assertTrue(testGame.getGameObjects() != null);
		assertTrue(!testGame.getGameObjects().isEmpty());
		assertTrue(testGame.getGameObjects().size() == 1);
		assertTrue(testGame.getGameObjects().elementAt(0) instanceof GameObject);
		assertTrue(((GameObject)testGame.getGameObjects().elementAt(0)).getGame() == testGame);
	}
	public void testRemoveGameObject() {		
		testGame.removeGameObject(testGameObject);
		assertTrue(!testGame.getGameObjects().contains(testGameObject));
	}		
	public void testStartSimulator() {
		testGame.startSimulator();
		
		assertTrue(testGame.getSimulatorThread() != null);
		assertTrue(testGame.getSimulatorThread().isAlive());
	}	
	public void testStopSimulator() {
		testGame.startSimulator();
		testGame.stopSimulator();
		
		try {
			Thread.sleep(Game.getSimulatorTickDelay() * 3);
		} catch (InterruptedException err) {}
		
		assertTrue(!testGame.getSimulatorThread().isAlive());
	}	
	public void update(java.util.Observable observable, java.lang.Object obj) {
		try {
			updateArgs[timesUpdateCalled] = obj;
		} catch (ArrayIndexOutOfBoundsException err) {}
		
		timesUpdateCalled++;
	}			
	public void testSimulateOneTick() {
		Rectangle initialBounds = new Rectangle(testGameObject.getShape().getBounds());
		testSimulatorGame.simulateOneTick(1000);
		
		assertTrue(!testGameObject.getShape().getBounds().equals(initialBounds));
		assertTrue(Math.round(testGameObject.getShape().getBounds().getX()) == Math.round(initialBounds.getX()) - 2);		
		assertTrue(timesUpdateCalled == 2);
		assertTrue(updateArgs[0] instanceof Boolean);
		assertTrue(((Boolean)updateArgs[0]).booleanValue() == Game.BEFORE_SIMULATE_CALLS);
		
		assertTrue(updateArgs[1] instanceof Boolean);
		assertTrue(((Boolean)updateArgs[1]).booleanValue() == Game.AFTER_SIMULATE_CALLS);
	}	
	public void testSetPlayingField() {
		Rectangle field = new Rectangle(0, 0, 50, 50);
		testGame.setPlayingField(field);
		assertTrue(testGame.getPlayingField().getBounds().equals(field));
	}	
	public void testSetBackgroundImage() {
		BufferedImage image = new BufferedImage(4, 4, BufferedImage.TYPE_INT_RGB);
		testGame.setBackgroundImage(image);
		assertTrue(testGame.getBackgroundImage() == image);
	}	
	public void testSetKeyListener() {
		KeyListener testKeyListener = new TestKeyListener();
		testGame.setKeyListener(testKeyListener);
		assertTrue(testGame.getKeyListener() == testKeyListener);
	}	
	public void testSimulator() {						
		long startTime = System.currentTimeMillis();
		
		Rectangle initialBounds = new Rectangle(testGameObject.getShape().getBounds());
		testSimulatorGame.startSimulator();
		
		try {
			Thread.sleep(Game.getSimulatorTickDelay() * 2);
		} catch (InterruptedException err) {
			err.printStackTrace();
		}
		
		assertTrue(!testGameObject.getShape().getBounds().equals(initialBounds));
		assertTrue(timesUpdateCalled > 0);
		assertTrue(Math.floor(timesUpdateCalled / 2) == timesUpdateCalled / 2);
	}	
	public void tearDown() {
		testGame.stopSimulator();
	}
	class TestKeyListener extends KeyAdapter {}
}

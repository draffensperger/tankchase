/*
 * ImageUtilitiesTest.java
 * JUnit based test
 *
 * Created on 20 January 2002, 18:13
 */                

package tankchase.test.unit;

import junit.framework.*;
import tankchase.util.*;
import java.awt.*;
import java.io.*;

/**
 *
 * @author Dave
 */                                
public class ImageUtilitiesTest extends TestCase {    
    public ImageUtilitiesTest(java.lang.String testName) {
        super(testName);
    }        
    public void testLoadImage1() {
		try {
			Image img = ImageUtilities.loadImage("", new Frame());
		} catch (IOException ioe) {
			assertTrue(!ioe.getMessage().equals(""));
			return;
		}		
		fail();
	}
	public void testLoadImage2() {
		try {
			Image img = ImageUtilities.loadImage(Class.class, "", new Frame());
		} catch (NullPointerException err) {
			fail();
		} catch (IOException ioe) {
			assertTrue(!ioe.getMessage().equals(""));
			return;
		}	
		fail();
	}
	public void testLoad() {
		try {
			Image img = Toolkit.getDefaultToolkit().createImage("");
			ImageUtilities.load(img, "name", new Frame());
		} catch (IOException ioe) {
			assertTrue(!ioe.getMessage().equals(""));
			return;
		}		
		fail();
	}
}

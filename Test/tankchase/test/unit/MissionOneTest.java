/*
 * MissionOneTest.java
 *
 * Created on December 2, 2001, 5:37 PM
 */

package tankchase.test.unit;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;
import java.util.*;
import java.awt.*;
import java.awt.geom.*;

/**
 *
 * @author  dave
 * @version 
 */
public class MissionOneTest extends TestCase {
	private MissionOne testMissionOne;
	private Component passedImageDestination = new Frame();
	
	public MissionOneTest(String name) {
		super (name);
	}	
	public void setUp() {
		try {
			testMissionOne = new MissionOne(passedImageDestination);
		} catch (NullPointerException err) {
			fail();
		}
	}
	public void testGetGameObjects() {
		assertTrue(testMissionOne.getGameObjects() != null);
		assertTrue(!testMissionOne.getGameObjects().isEmpty());
		
		for (int i = 0; i < testMissionOne.getGameObjects().size(); i++) {
			Object currentObject = testMissionOne.getGameObjects().elementAt(i);
			
			assertTrue(currentObject != null);
			assertTrue(currentObject instanceof GameObject);
			assertTrue(((GameObject)currentObject).getShape() != null);
			assertTrue(((GameObject)currentObject).getShape().getBounds().getHeight() > 0);
			assertTrue(((GameObject)currentObject).getShape().getBounds().getWidth() > 0);
			assertTrue(((GameObject)currentObject).getColor() != null);
		}
	}	
	public void testConstructor() {
		assertTrue(testMissionOne.getImageDestination() == passedImageDestination);
	}
	public void testSetGameObjects() {
		Vector gameObjects = new Vector();
		gameObjects.addElement(new GameObject());
		
		testMissionOne.setGameObjects(gameObjects);
		
		assertTrue(!testMissionOne.getGameObjects().isEmpty());
		assertTrue(testMissionOne.getGameObjects().equals(gameObjects));
	}	
	public void testGetNumPlayers() {
		assertTrue(testMissionOne.getNumPlayers() > 1);
	}
	public void testGetHumanTanks() {
		assertTrue(testMissionOne.getHumanTanks() != null);
		
		for (int i = 0; i < testMissionOne.getHumanTanks().size(); i++) {
			assertTrue(testMissionOne.getHumanTanks().elementAt(i) instanceof Tank);
			Tank tank = (Tank)testMissionOne.getHumanTanks().elementAt(i);
			assertTrue(tank != null);
			assertTrue(testMissionOne.getGameObjects().contains(tank));
			assertTrue(tank.getColor() != null);
			assertTrue(tank.getShape() != null);
			assertTrue(tank.getBulletPrototype() != null);
			assertTrue(tank.getBulletPrototype().getShape() != null);
			assertTrue(tank.getBulletPrototype().getColor() != null);
			assertTrue(tank.getBulletPrototype().getSpeed() != 0);
			assertTrue(tank.getExplosionPrototype() != null);
			assertTrue(tank.getExplosionPrototype().getShape() != null);
			assertTrue(tank.getExplosionPrototype().getColor() != null);
			assertTrue(tank.getExplosionPrototype().getSpeed() == 0);
		}
	}	
	public void testGetPlayingField() {
		assertNotNull(testMissionOne.getPlayingField());
		Rectangle2D fieldBounds = testMissionOne.getPlayingField().getBounds();
		assertTrue(fieldBounds.getWidth() > 0);
		assertTrue(fieldBounds.getHeight() > 0);
		assertTrue(fieldBounds.getX() == 0);
		assertTrue(fieldBounds.getY() == 0);
		assertTrue(new Area(fieldBounds).isRectangular());
	}
	public void testFiredBulletSpeed() {
		Tank player1 = ((Tank)testMissionOne.getHumanTanks().elementAt(0)); 
		player1.setSpeed(50);
		player1.getBulletPrototype().setSpeed(5);
		player1.fire();
		Bullet firedBullet = (Bullet)testMissionOne.getGameObjects().lastElement();
		assertTrue(firedBullet.getSpeed() == 55);
	}
	public void testGetKeyListener() {
		assertTrue(testMissionOne.getKeyListener() != null);
	}
}

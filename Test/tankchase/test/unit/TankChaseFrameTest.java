/*
 * TankChaseFrameTest.java
 *
 * Created on December 2, 2001, 5:36 PM
 */

package tankchase.test.unit;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;
import tankchase.ui.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.event.*;

/**
 *
 * @author  dave
 * @version 
 */
public class TankChaseFrameTest extends TestCase {
	private TankChaseFrame testFrame;
	
	public TankChaseFrameTest(String name) {
		super(name);
	}	
	public void setUp() {
		testFrame = new TankChaseFrame();	
	}	
	public void testAttributes() {
		assertTrue(testFrame.getTitle() != "");
		assertTrue(!testFrame.isResizable());
	}
	public void testGetKeyListener() {
		assertTrue(testFrame.getKeyListener() == null);
		Game testGame = new Game();
		KeyListener testListener = new TestKeyListener();
		testGame.setKeyListener(testListener);
		testFrame.setGame(testGame);
		assertTrue(testFrame.getKeyListener() == testListener);
	}
	public void testNoPointerExceptionWhenBgNull() {
		Game testGame = new Game();
		testGame.setBackgroundImage(null);
		BufferedImage img =
				new BufferedImage(10, 10, BufferedImage.TYPE_BYTE_GRAY);		
		testFrame.setGame(testGame);
		try {
			testFrame.getGameObjectsDisplayer().paint(img.getGraphics());
		} catch (NullPointerException err) {
			fail();
		}
	}
	public void testNoPointerExceptionWhenBgNullAndOldShapes() {
		Game testGame = new Game();
		GameObject testGameObject = new GameObject();
		testGame.setPlayingField(new Rectangle(0, 0, 50, 50));
		testGameObject.setShape(new Rectangle(20, 20, 5, 5));
		testGameObject.setSpeed(5);
		testGame.addGameObject(testGameObject);
		testGame.setBackgroundImage(null);		
		testFrame.setGame(testGame);
		
		testGame.simulateOneTick(1000);
		
		BufferedImage img =
				new BufferedImage(10, 10, BufferedImage.TYPE_BYTE_GRAY);		
		try {
			testFrame.getGameObjectsDisplayer().paint(img.getGraphics());
		} catch (NullPointerException err) {
			fail();
		}
	}
	public void testDisplayer() {
		assertTrue(testFrame.getGameObjectsDisplayer() != null);
		
		Game testGame = new Game();
		testGame.setPlayingField(new Rectangle(0, 0, 20, 20));
		testFrame.setGame(testGame);
		
		boolean isDisplayerContained = false;
		for (int i = 0; i < testFrame.getComponentCount(); i++) {
			if (testFrame.getGameObjectsDisplayer() == testFrame.getContentPane().getComponent(i)) {
				isDisplayerContained = true;
				break;
			}
		}
		assertTrue(isDisplayerContained);
		assertTrue(testFrame.getGameObjectsDisplayer().getSize().width > 0);
		assertTrue(testFrame.getGameObjectsDisplayer().getSize().height > 0);
	}
	public void testSetGame() {
		MissionOne testGame = new MissionOne(testFrame);
		testFrame.setGame(testGame);
		
		assertTrue(testFrame.getGame() != null);
		assertTrue(testFrame.getGame().getGameObjects().equals(testGame.getGameObjects()));

		assertTrue(testFrame.getBounds().getWidth() >= testGame.getPlayingField().getBounds().getWidth());
		assertTrue(testFrame.getBounds().getHeight() >= testGame.getPlayingField().getBounds().getHeight());

		Game gameWithNoPlayingField = new Game();
		gameWithNoPlayingField.setPlayingField(null);

		try {
			testFrame.setGame(gameWithNoPlayingField);
		} catch (NullPointerException err) {
			fail();
		}
	}	
	public void testUpdateBeforeSimulatedArg() {
		Game testGame = new Game();
		GameObject testObject = new GameObject();
		
		testObject.setShape(new Rectangle(0, 0, 50, 50));
		Rectangle originalShape = new Rectangle((Rectangle)testObject.getShape());		
		
		testGame.addGameObject(testObject);
		testFrame.setGame(testGame);
		testFrame.update(null, new Boolean(Game.BEFORE_SIMULATE_CALLS));
		
		testObject.setShape(new Rectangle(5, 5, 55, 55));
		
		assertTrue(testFrame.getOldShapes().length == 1);
		assertTrue(testFrame.getOldShapes()[0].equals(originalShape));
	}	
	public void testUpdateAfterSimulatedArg() {
		Game testGame = new Game();
		GameObject testObject = new GameObject();
		
		testObject.setShape(new Rectangle(0, 0, 50, 50));
		
		testGame.addGameObject(testObject);
		testFrame.setGame(testGame);
				
		testFrame.update(null, new Boolean(Game.AFTER_SIMULATE_CALLS));
					
		assertTrue(testFrame.getOldShapes() == null);						
	}
	public void testGetGameObjectsDisplayer() {
		Game testGame = new Game();
		testGame.setPlayingField(new Rectangle(0, 0, 50, 50));
		testFrame.setGame(testGame);
		JComponent displayer = testFrame.getGameObjectsDisplayer();
		
		try {
			(new Robot()).waitForIdle();
			Thread.sleep(3000);
			testFrame.validate();
		} catch (Exception err) {
			err.printStackTrace();
		}
		assertTrue(displayer != null);
		assertTrue(Math.abs(displayer.getPreferredSize().width - testGame.getPlayingField().getBounds().getWidth()) < 0.001);
		assertTrue(Math.abs(displayer.getPreferredSize().height - testGame.getPlayingField().getBounds().getHeight()) < 0.001);
	}
	class TestKeyListener extends KeyAdapter {}
}

/*
 * GameTest.java
 *
 * Created on December 2, 2001, 5:37 PM
 */

package tankchase.test.unit;

import junit.framework.*;
import junit.extensions.*;
import tankchase.data.*;
import java.awt.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.util.*;

/**
 *
 * @author  dave
 * @version 
 */
public class TankTest extends TestCase {
	private Game testGame;
	private Tank testTank;
		
	public TankTest(String name) {
		super (name);
	}
	public void setUp() {
		testTank = new Tank();
		testTank.setShape(new Rectangle(0, 0, 10, 50));
		testGame = new Game();
		testGame.setPlayingField(new Rectangle(0, 0, 200, 200));
		testGame.addGameObject(testTank);
	}
	public void testBulletKills() {
		Bullet testBullet = new Bullet();
		testGame.addGameObject(testBullet);
		
		testTank.setShape(new Rectangle(0, 0, 10, 10));
		testBullet.setShape(new Rectangle(5, 5, 5, 5));
		testGame.simulateOneTick(0);
		assertTrue(!testGame.getGameObjects().contains(testTank));
	}
	public void testSetExplosionPrototype() {
		Explosion explosion = new Explosion(100);
		testTank.setExplosionPrototype(explosion);
		assertTrue(testTank.getExplosionPrototype() == explosion);
	}
	public void testExplosionAppearsOnTankDeath() {
		Bullet testBullet = new Bullet();
		testGame.addGameObject(testBullet);
		testTank.setShape(new Rectangle(0, 0, 10, 10));
		testBullet.setShape(new Rectangle(5, 5, 5, 5));
		
		Explosion explosionPrototype = new Explosion(500);
		Rectangle initialRect = new Rectangle(0, 0, 50, 50);
		explosionPrototype.setShape(new Rectangle(initialRect));
		testTank.setExplosionPrototype(explosionPrototype);				
		
		testGame.simulateOneTick(0);
		assertTrue(testGame.getGameObjects().lastElement() 
				   instanceof Explosion);
		Explosion explosion = 
				(Explosion) testGame.getGameObjects().lastElement();
		assertTrue(explosion.getShape().getBounds().width == initialRect.width);
		assertTrue(explosion.getShape().getBounds().height 
				   == initialRect.height);
		assertTrue(explosion != explosionPrototype);
		assertTrue(Math.abs(explosion.getShape().getBounds2D().getCenterX() -
				testTank.getShape().getBounds2D().getCenterX()) < 0.0001);
		assertTrue(Math.abs(explosion.getShape().getBounds2D().getCenterY() -
				testTank.getShape().getBounds2D().getCenterY()) < 0.0001);
	}
	public void testNoPointerExceptionWhenNullExplosion() {
		Bullet testBullet = new Bullet();
		testGame.addGameObject(testBullet);
		testTank.setShape(new Rectangle(0, 0, 10, 10));
		testBullet.setShape(new Rectangle(5, 5, 5, 5));
		
		testTank.setExplosionPrototype(null);		
		try {
			testGame.simulateOneTick(0);		
		} catch (NullPointerException err) {
			fail();
		}
	}
	public void testTankDoesntKillOtherTank() {
		Bullet testBullet = new Bullet();
		Tank testOtherTank = new Tank();
		testGame.addGameObject(testOtherTank);
		
		testTank.setShape(new Rectangle(0, 0, 10, 10));
		testOtherTank.setShape(new Rectangle(5, 5, 5, 5));
		testGame.simulateOneTick(0);
		assertTrue(testGame.getGameObjects().contains(testTank));
		assertTrue(testGame.getGameObjects().contains(testOtherTank));
	}
	public void testTankStaysInField() {
		  testTank.setShape(new Rectangle(-10, -10, 20, 20));
		  testTank.simulate(10);
		  assertTrue(testTank.isFullyWithinPlayingField());		  
    }
	public void testFireWithoutBulletPrototype() {
		testTank.setBulletPrototype(null);
		try {
			testTank.fire();
		} catch (IllegalThreadStateException err) {
			assertTrue(!err.getMessage().equals(""));
			return;
		}
		fail();
	}	
	public void testFireWithBulletPrototype() {		
		int oldNumGameObjects = testGame.getGameObjects().size();
		Bullet prototype = new Bullet();
		prototype.setShape(new Rectangle(30, 30, 20, 10));
		testTank.setBulletPrototype(prototype);
		testTank.setFacingDirection(Math.PI / 2);
		testTank.setShape(new Rectangle(50, 50, 5, 5));
		testTank.fire();
		testGame.setPlayingField(new Rectangle(0, 0, 100, 100));
		testGame.simulateOneTick(0);

		assertTrue(testGame.getGameObjects().size() == oldNumGameObjects + 1);
		assertTrue(testGame.getGameObjects().lastElement() instanceof Bullet);

		Bullet firedBullet = (Bullet)testGame.getGameObjects().lastElement();
		assertTrue(firedBullet.getDirection() == testTank.getFacingDirection());
		assertTrue(firedBullet.getFacingDirection() == testTank.getFacingDirection());
		assertTrue(firedBullet.getSpeed() == prototype.getSpeed() + testTank.getSpeed());
		assertTrue(Math.abs(firedBullet.getShape().getBounds2D().getWidth() 
						    - 10) < 0.0001);
		assertTrue(Math.abs(firedBullet.getShape().getBounds2D().getHeight() 
						    - 20) < 0.0001);

		Area firedBulletArea = new Area(firedBullet.getShape());
		Area tankArea = new Area(testTank.getShape());
		Area expectedFiredBulletArea = new Area(firedBulletArea);		
		firedBulletArea.subtract(tankArea);
		assertTrue(firedBulletArea.equals(expectedFiredBulletArea));
	}
	public void testSetBulletPrototype() {
		Bullet testPrototype = new Bullet();
		testPrototype.setColor(Color.red);
		testPrototype.setShape(new Rectangle(0, 0, 50, 50));
		testPrototype.setSpeed(2);
		testTank.setBulletPrototype(testPrototype);
		testTank.setSpeed(5);
		testTank.fire();

		Bullet firedBullet = (Bullet)testGame.getGameObjects().lastElement();		
		assertTrue(firedBullet != testPrototype);
		Rectangle2D firedBulletBoundsShifted = AffineTransform.getTranslateInstance(
			testPrototype.getShape().getBounds2D().getX() - firedBullet.getShape().getBounds2D().getX(), 
			testPrototype.getShape().getBounds2D().getY() - firedBullet.getShape().getBounds2D().getY()
			).createTransformedShape(firedBullet.getShape()).getBounds2D();
		assertTrue(Math.abs(firedBulletBoundsShifted.getWidth() - firedBullet.getShape().getBounds2D().getWidth()) < 0.001);
		assertTrue(Math.abs(firedBulletBoundsShifted.getHeight() - firedBullet.getShape().getBounds2D().getHeight()) < 0.001);
		assertTrue(firedBullet.getSpeed() == testPrototype.getSpeed() + testTank.getSpeed());
		assertTrue(firedBullet.getColor().equals(testPrototype.getColor()));

		double slopeBetweenBulletAndTank = 
			(firedBullet.getShape().getBounds2D().getCenterY() - testTank.getShape().getBounds2D().getCenterY()) / 
			(firedBullet.getShape().getBounds2D().getCenterX() - testTank.getShape().getBounds2D().getCenterX()); 
		assertTrue(Math.abs(Math.tan(slopeBetweenBulletAndTank) - testTank.getDirection()) < 0.001);
	}
	public void testGetBulletPrototype() {
		Bullet testPrototype = new Bullet();
		testTank.setBulletPrototype(testPrototype);

		assertTrue(testPrototype == testTank.getBulletPrototype());
	}
	public void testRotate() {
		testTank.setDirection(0);
		Shape originalShape = new Area(testTank.getShape());
		testTank.setFacingDirection(0);
		testTank.rotate(Math.PI / 2);
		assertTrue(testTank.getDirection() == Math.PI / 2);
		assertTrue(testTank.getFacingDirection() == Math.PI / 2);
		testGame.simulateOneTick(0);
		assertTrue(Math.abs(testTank.getShape().getBounds2D().getWidth() - originalShape.getBounds2D().getHeight()) < 0.0001);
	}
}
